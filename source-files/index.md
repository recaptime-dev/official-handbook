---
title: English Handbok Main Page
summary: The main page of the Pins team's official English Handbook.
---

Welcome to the Pins team's official English Handbook! This is where the whole handbook lives and resides, mostly adopted from GitLab Handbook.

## The handbook is an open-sourced work in progress project, so everyone can contribute.
Your contributions are licensed under both Mozilla Public License Version 2.0+ and Creative Commons Attribution-ShareAlike International License 4.0+.

You are free to fork, remix, and use our handbook in any form, but you are required to give attribution and license under the same terms, unless otherwise specified in your IP permission request in the Issue Tracker.
